1.vue-router 使用 pushState 进行路由更新，静态跳转，页面不会重新加载；location.href 会触发浏览器页面重新加载一次
2.vue-router 使用 diff 算法，实现按需加载，减少 DOM 操作；
3.vue-router 是路由跳转或同一个页面跳转；location.href 是不同页面间跳转；
4.vue-router 是异步加载 this.$nextTick(()=>{}),location-href 是同步加载；

```js
this.$router.push({
    path:'./fillInfomation',
    query:{
        applicationNo:this.applicationNo,
        contractNo:this.contractNo
    }
});
//页面跳转后获取携带参数applicationNo，contractNo
const applicationNo = this.$route.query.applicationNo;
const contractNo = this.$route.query.contractNo;
//此用法参数会展示在跳转地址上
```



```js
this.$router.push({
    name:'clientDetail',
    params:{
        clientCode:this.clientCode,
        clientType:this.clientType
    }
});
//页面跳转后获取携带参数clientCode，clientType
const clientCode = this.$route.params.clientCode;
const clientType = this.$route.params.clientType;
//此用法参数不会展示在跳转地址上
```

