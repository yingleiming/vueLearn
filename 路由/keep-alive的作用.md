keep-alive 的作用
总的作用就是：缓存组件，防止二次渲染，能够大大节省性能。

keep-alive的基本用法：

```js
<!---缓存所有的页面--->
<keep-alive>
    <router-view v-if="$route.meta.keep_alive"></router-view>
</keep-alive>
<router-view v-if="!$route.meta.keep_alive"></router-view>
```

需要缓存的组件内容直接在router中添加：

```js
meta:{
	keepAlive:true;//true 表示需要使用缓存，false 表示不需要使用缓存
}
```

keep-alive的生命周期

当引入keep-alive的时候，页面第一次进入，钩子的触发顺序created--->mounted--->activated，退出时触发deactivated。当再次进入时（前进或者后退），只触发activated

使用场景：

在做电商有关的项目中，当我们第一次进入列表页需要请求一下数据，当我从列表页进入详情页，详情页不缓存也需要请求下数据，然后返回列表页，这时候我们使用keep-alive来缓存组件，防止二次渲染，这样会大大的节省性能。
