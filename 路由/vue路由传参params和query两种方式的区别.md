```js
this.$router.push({
    path:'./fillInfomation',
    query:{
        applicationNo:this.applicationNo,
        contractNo:this.contractNo
    }
});
//页面跳转后获取携带参数applicationNo，contractNo
const applicationNo = this.$route.query.applicationNo;
const contractNo = this.$route.query.contractNo;
//此用法参数会展示在跳转地址上
```



```js
this.$router.push({
    name:'clientDetail',
    params:{
        clientCode:this.clientCode,
        clientType:this.clientType
    }
});
//页面跳转后获取携带参数clientCode，clientType
const clientCode = this.$route.params.clientCode;
const clientType = this.$route.params.clientType;
//此用法参数不会展示在跳转地址上
```

